## Q-SYS Morse Code Generator

This example script controls a sine generator to create morse code from an inputted string. 

An example .qsys file is included in the Downloads section.

Setup Instructions:  

1. Add a text controller to your design  
2. Add the following controls to the text controller:  
	- message (Text Box)  
	- trigger (Trigger Button)  
	- ToneGeneratorName (Combo Box)  
3. Add a sine generator to your design  
4. Name the sine generator component  
5. Load design to Core and select the sine generator in the **ToneGeneratorName** dropdown  